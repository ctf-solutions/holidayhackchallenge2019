#!/usr/bin/env python3

import time
from selenium import webdriver

options = webdriver.ChromeOptions()
options.add_argument("load-extension=chrome-extension")
driver = webdriver.Chrome(options=options)

driver.get('https://sleighworkshopdoor.elfu.org/')