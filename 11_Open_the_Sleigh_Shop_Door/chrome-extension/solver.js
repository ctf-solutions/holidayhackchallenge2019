function solve(n, code) {
    document.querySelector(`input[data-id='${n}']:not(.hint-dispenser)`).value = code;
    document.querySelector(`button[data-id='${n}']:not(.hint-dispenser)`).disabled = false;
    document.querySelector(`button[data-id='${n}']:not(.hint-dispenser)`).click();
}

var CODE_1 = '';
var CODE_3 = '';
const log = console.log;
const f = window.fetch;
(function() {
    console.log = function() {
        if (typeof arguments[0] === 'string') {
            m = arguments[0].match(/([A-Z0-9]{8})/g);
            if (m) {
                CODE_1 = m[0];
            }
        }
        log.apply(this, Array.prototype.slice.call(arguments));
    };

    window.fetch = function() {
        return new Promise((resolve, reject) => {
            f.apply(this, arguments).then((response) => {
                if (arguments[0].endsWith('.png')) {
                    console.log(response.url);
                    response.blob().then((image) => {
                        const { createWorker } = Tesseract;
                        (async () => {
                            const worker = createWorker();
                            await worker.load();
                            await worker.loadLanguage('eng');
                            await worker.initialize('eng');
                            await worker.setParameters({
                                tessedit_char_whitelist: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
                            });
                            const { data: { text } } = await worker.recognize(image);
                            CODE_3 = text.trim();
                            var event = new Event('code3ready');
                            window.dispatchEvent(event);
                        })();
                    });
                }
                resolve(response);
            });
          });
       }
}());

window.addEventListener('code3ready', solve_all, false);

function solve_all() {
    console.log('Solving all');
    solve(1, CODE_1);
    solve(2, document.querySelector("div.libra strong").innerText);
    solve(3, CODE_3);
    solve(4, localStorage.getItem('🛢️🛢️🛢️'));
    solve(5, document.title.slice(document.title.length - 8));
    solve(6, [4, 1, 5, 7, 6, 3, 8, 2].map(n => document.querySelector(`div.hologram div.items div:nth-child(${n})`).innerText).join(''));
    solve(7, getComputedStyle(document.querySelector('.instructions')).fontFamily.split(',')[0].replace(/^"+|"+$/gm,''));
    solve(8, 'VERONICA');
    solve(9, Array.prototype.slice.call(document.styleSheets[0].rules).filter(rule => rule.selectorText?rule.selectorText.startsWith('span.chakra:nth-child'):false).map(rule => rule.style.content).join('').replace(/"/gm,''));
    document.querySelector('.lock.c10').appendChild(document.querySelector('.macaroni'));
    document.querySelector('.lock.c10').appendChild(document.querySelector('.swab'));
    document.querySelector('.lock.c10').appendChild(document.querySelector('.gnome'));
    solve(10, 'KD29XJ37');
}

