#!/usr/bin/env python3

import sys
import Evtx.Evtx as evtx
from lxml import etree
from pprint import pprint

def get_child(node, tag, ns="{http://schemas.microsoft.com/win/2004/08/events/event}"):
    return node.find("%s%s" % (ns, tag))

def get_all_children(node, tag, ns="{http://schemas.microsoft.com/win/2004/08/events/event}"):
    return node.findall("%s%s" % (ns, tag))

with evtx.Evtx(sys.argv[1]) as log:
    successful = []
    unsuccessful = dict()
    for record in log.records():
        event = record.lxml()
        system = get_child(event, 'System')
        event_id = get_child(system, 'EventID')
        # Search for successful and unsuccessful login attempt events
        if int(event_id.text) == 4624 or int(event_id.text) == 4625:
            event_data = get_child(event, 'EventData')
            ip_address = None
            target_user_name = None
            for data in get_all_children(event_data, 'Data'):
                if data.get('Name') == 'IpAddress':
                    ip_address = data.text
                if data.get('Name') == 'TargetUserName':
                    target_user_name = data.text

            # For successful login events, store TargetUserName and IpAddress
            if int(event_id.text) == 4624:
                successful.append({'IpAddress': ip_address, 'TargetUserName': target_user_name})
            # For unsuccessful login attempts, count the number of events per ip address
            if int(event_id.text) == 4625:
                if ip_address not in unsuccessful:
                    unsuccessful[ip_address] = 1
                else:
                    unsuccessful[ip_address] += 1
        #print(etree.tostring(event, pretty_print=True, encoding="unicode"))
    #pprint(successful)
    #pprint(unsuccessful)

    # Attacker used password spray attack, so his IP address is the one with the highest number of unsuccessful login attempts
    attacker_ip = next(iter({k: v for k, v in sorted(unsuccessful.items(), key=lambda item: item[1])}))
    # Return the list of usernames for which attacker logged-in successfully
    for entry in successful:
        if entry['IpAddress'] == attacker_ip:
            print(entry['TargetUserName'])
