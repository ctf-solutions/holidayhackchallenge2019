# Escape Ed

From Bushy Evergreen.

Quit ed. Just press `q`.

```
                 ........................................
               .;oooooooooooool;,,,,,,,,:loooooooooooooll:
             .:oooooooooooooc;,,,,,,,,:ooooooooooooollooo:
           .';;;;;;;;;;;;;;,''''''''';;;;;;;;;;;;;,;ooooo:
         .''''''''''''''''''''''''''''''''''''''''';ooooo:
       ;oooooooooooool;''''''',:loooooooooooolc;',,;ooooo:
    .:oooooooooooooc;',,,,,,,:ooooooooooooolccoc,,,;ooooo:
  .cooooooooooooo:,''''''',:ooooooooooooolcloooc,,,;ooooo,
  coooooooooooooo,,,,,,,,,;ooooooooooooooloooooc,,,;ooo,
  coooooooooooooo,,,,,,,,,;ooooooooooooooloooooc,,,;l'
  coooooooooooooo,,,,,,,,,;ooooooooooooooloooooc,,..
  coooooooooooooo,,,,,,,,,;ooooooooooooooloooooc.
  coooooooooooooo,,,,,,,,,;ooooooooooooooloooo:.
  coooooooooooooo,,,,,,,,,;ooooooooooooooloo;
  :llllllllllllll,'''''''';llllllllllllllc,
Oh, many UNIX tools grow old, but this one's showing gray.
That Pepper LOLs and rolls her eyes, sends mocking looks my way.
I need to exit, run - get out! - and celebrate the yule.
Your challenge is to help this elf escape this blasted tool.
-Bushy Evergreen
Exit ed.
1100
q
Loading, please wait......
You did it! Congratulations!
```

# Linux Path

From SugarPlum Mary.

List the current directory. There're multiple `ls` commands on the `PATH`. Use full path: `/bin/ls`.

```
K000K000K000KK0KKKKKXKKKXKKKXKXXXXXNXXXX0kOKKKK0KXKKKKKKK0KKK0KK0KK0KK0KK0KK0KKKKKK
00K000KK0KKKKKKKKKXKKKXKKXXXXXXXXNXXNNXXooNOXKKXKKXKKKXKKKKKKKKKK0KKKKK0KK0KK0KKKKK
KKKKKKKKKKKXKKXXKXXXXXXXXXXXXXNXNNNNNNK0x:xoxOXXXKKXXKXXKKXKKKKKKKKKKKKKKKKKKKKKKKK
K000KK00KKKKKKKKXXKKXXXXNXXXNXXNNXNNNNNWk.ddkkXXXXXKKXKKXKKXKKXKKXKKXK0KK0KK0KKKKKK
00KKKKKKKKKXKKXXKXXXXXNXXXNXXNNNNNNNNWXXk,ldkOKKKXXXXKXKKXKKXKKXKKKKKKKKKK0KK0KK0XK
KKKXKKKXXKXXXXXNXXXNXXNNXNNNNNNNNNXkddk0No,;;:oKNK0OkOKXXKXKKXKKKKKKKKKKKKK0KK0KKKX
0KK0KKKKKXKKKXXKXNXXXNXXNNXNNNNXxl;o0NNNo,,,;;;;KWWWN0dlk0XXKKXKKXKKXKKKKKKKKKKKKKK
KKKKKKKKXKXXXKXXXXXNXXNNXNNNN0o;;lKNNXXl,,,,,,,,cNNNNNNKc;oOXKKXKKXKKXKKXKKKKKKKKKK
XKKKXKXXXXXXNXXNNXNNNNNNNNN0l;,cONNXNXc',,,,,,,,,KXXXXXNNl,;oKXKKXKKKKKK0KKKKK0KKKX
KKKKKKXKKXXKKXNXXNNXNNNNNXl;,:OKXXXNXc''',,''''',KKKKKKXXK,,;:OXKKXKKXKKX0KK0KK0KKK
KKKKKKKKXKXXXXXNNXXNNNNW0:;,dXXXXXNK:'''''''''''cKKKKKKKXX;,,,;0XKKXKKXKKXKKK0KK0KK
XXKXXXXXXXXXXNNNNNNNNNN0;;;ONXXXXNO,''''''''''''x0KKKKKKXK,',,,cXXKKKKKKKKXKKK0KKKX
KKKKKKKXKKXXXXNNNNWNNNN:;:KNNXXXXO,'.'..'.''..':O00KKKKKXd'',,,,KKXKKXKKKKKKKKKKKKK
KKKKKXKKXXXXXXXXNNXNNNx;cXNXXXXKk,'''.''.''''.,xO00KKKKKO,'',,,,KK0XKKXKKK0KKKKKKKK
XXXXXXXXXKXXXXXXXNNNNNo;0NXXXKKO,'''''''.'.'.;dkOO0KKKK0;.'',,,,XXXKKK0KK0KKKKKKKKX
XKKXXKXXXXXXXXXXXNNNNNcoNNXXKKO,''''.'......:dxkOOO000k,..''',,lNXKXKKXKKK0KKKXKKKK
KXXKKXXXKXXKXXXXXXXNNNoONNXXX0;'''''''''..'lkkkkkkxxxd'...'''',0N0KKKKKXKKKKKK0XKKK
XXXXXKKXXKXXXXXXXXXXXXOONNNXXl,,;;,;;;;;;;d0K00Okddoc,,,,,,,,,xNNOXKKKKKXKKKKKKKXKK
XXXXXXXXXXXXXXXXXXXXXXXONNNXx;;;;;;;;;,,:xO0KK0Oxdoc,,,,,,,,,oNN0KXXKKXKKXKKKKKKKXK
XKXXKXXXXXXXXXXXXXXXXXXXXWNX:;;;;;;;;;,cO0KKKK0Okxl,,,,,,,,,oNNK0NXXXXXXXXXKKKKKKKX
XXXXXXXXXXXXXXXXXXXXXXXNNNWNc;;:;;;;;;xKXXXXXXKK0x,,,,,,,,,dXNK0NXXXXXXXXXXXKKXKKKK
XKXXXXXXXXXXXXXXXXXXXXNNWWNWd;:::;;;:0NNNNNNNNNXO;,,,,,,,:0NN0XNXNXXXXXXXXXXXKKXKKX
NXXXXXXXXXXXXXXXXXXXXXNNNNNNNl:::;;:KNNNNNNNNNNO;,,,,,,;xNNK0NXNXXNXXXXXXKXXKKKKXKK
XXNNXNNNXXXXXXXXXXXXXNNNNNNNNNkl:;;xWWNNNNNWWWk;;;;;;;xNNKKXNXNXXNXXXXXXXXXXXKXKKXK
XXXXXNNNNXNNNNXXXXXXNNNNNNNNNNNNKkolKNNNNNNNNx;;;;;lkNNXNNNNXXXNXXNXXXXXXXXXXXKKKKX
XXXXXXXXXXXNNNNNNNNNNNNNNNNNNNNNNNNNKXNNNNWNo:clxOXNNNNNNNNXNXXXXXXXXXXXXXXXKKXKKKK
XXXXNXXXNXXXNXXNNNNNWWWWWNNNNNNNNNNNNNNNNNWWNWWNWNNWNNNNNNNNXXXXXXNXXXXXXXXXXKKXKKX
XNXXXXNNXXNXXNNXNXNWWWWWWWWWNNNNNNNNNNNNNWWWWNNNNNNNNNNNNNNNNNNNNNXNXXXXNXXXXXXKXKK
XXXXNXXNNXXXNXXNXXNWWWNNNNNNNNNWWNNNNNNNNWWWWWWNWNNNNNNNNNNNNNNNXXNXNXXXXNXXXXKXKXK
I need to list files in my home/
To check on project logos
But what I see with ls there,
Are quotes from desert hobos...
which piece of my command does fail?
I surely cannot find it.
Make straight my path and locate that-
I'll praise your skill and sharp wit!
Get a listing (ls) of your current directory.
elf@52b91fff80fd:~$ ls
This isn't the ls you're looking for
elf@52b91fff80fd:~$ which ls
/usr/local/bin/ls
elf@52b91fff80fd:~$ /bin/ls  
' '   rejected-elfu-logos.txt
Loading, please wait......
You did it! Congratulations!
elf@52b91fff80fd:~$
```

# Nyanshell

From Alabaster Snowball.

Check in `/etc/passwd` that the login shell for `alabaster_snowball` user is `/bin/nsh`.
Check that it's writable by everyone, but has `immutable` attribute set.
With `sudo -l` check that you can execute `chattr` as root.
Remove the `immutable` attribute from `/bin/nsh` and replace it with contents of `/bin/bash`.
Switch to `alabaster_snowball` by using `su`.

```
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
░░░░░░░░░░▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄░░░░░░░░░
░░░░░░░░▄▀░░░░░░░░░░░░▄░░░░░░░▀▄░░░░░░░
░░░░░░░░█░░▄░░░░▄░░░░░░░░░░░░░░█░░░░░░░
░░░░░░░░█░░░░░░░░░░░░▄█▄▄░░▄░░░█░▄▄▄░░░
░▄▄▄▄▄░░█░░░░░░▀░░░░▀█░░▀▄░░░░░█▀▀░██░░
░██▄▀██▄█░░░▄░░░░░░░██░░░░▀▀▀▀▀░░░░██░░
░░▀██▄▀██░░░░░░░░▀░██▀░░░░░░░░░░░░░▀██░
░░░░▀████░▀░░░░▄░░░██░░░▄█░░░░▄░▄█░░██░
░░░░░░░▀█░░░░▄░░░░░██░░░░▄░░░▄░░▄░░░██░
░░░░░░░▄█▄░░░░░░░░░░░▀▄░░▀▀▀▀▀▀▀▀░░▄▀░░
░░░░░░█▀▀█████████▀▀▀▀████████████▀░░░░
░░░░░░████▀░░███▀░░░░░░▀███░░▀██▀░░░░░░
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
nyancat, nyancat
I love that nyancat!
My shell's stuffed inside one
Whatcha' think about that?
Sadly now, the day's gone
Things to do!  Without one...
I'll miss that nyancat
Run commands, win, and done!
Log in as the user alabaster_snowball with a password of Password2, and land in a Bash pro
mpt.
Target Credentials:
username: alabaster_snowball
password: Password2
elf@0a349470ca3c:~$ cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
elf:x:1000:1000::/home/elf:/bin/bash
alabaster_snowball:x:1001:1001::/home/alabaster_snowball:/bin/nsh
elf@0a349470ca3c:~$ ls -al /bin/nsh
-rwxrwxrwx 1 root root 75680 Dec 11 17:40 /bin/nsh
elf@0a349470ca3c:~$ lsattr /bin/nsh
----i---------e---- /bin/nsh
elf@0a349470ca3c:~$ sudo -l
Matching Defaults entries for elf on 7ff90b6b1e75:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin
User elf may run the following commands on 7ff90b6b1e75:
    (root) NOPASSWD: /usr/bin/chattr
elf@0a349470ca3c:~$ sudo chattr -i /bin/nsh
elf@0a349470ca3c:~$ cat /bin/bash >/bin/nsh
elf@0a349470ca3c:~$ su - alabaster_snowball
Password: 
Loading, please wait......
You did it! Congratulations!
alabaster_snowball@0a349470ca3c:~$
```

# Mongo Pilfer

From Holly Evergreen.

Execute `ps` to see the `mongod` commandline arguments, including the port (12121).
Connect to MongoDB instance.
List datbases and connect to `elfu` DB.
List all collections and list contents of `solution` collection.
It reads to run `db.loadServerScripts();displaySolution();`.

```
'...',...'::'''''''''cdc,',,,,,,,cxo;,,,,,,,,:dl;,;;:;;;;;l:;;;cx:;;:::::lKXkc::
oc;''.',coddol;''';ldxxxxoc,,,:oxkkOkdc;,;:oxOOOkdc;;;:lxO0Oxl;;;;:lxOko::::::cd
ddddocodddddddxxoxxxxxkkkkkkxkkkkOOOOOOOxkOOOOOOO00Oxk000000000xdk00000K0kllxOKK
coddddxxxo::ldxxxxxxdl:cokkkkkOkxl:lxOOOOOOOkdlok0000000Oxok00000000OkO0KKKKKKKK
'',:ldl:,'''',;ldoc;,,,,,,:oxdc;,,,;;;cdOxo:;;;;;:ok0kdc;;;;:ok00kdc:::lx0KK0xoc
oc,''''';cddl:,,,,,;cdkxl:,,,,,;lxOxo:;;;;;:ldOxl:;;:;;:ldkoc;;::;;:oxo:::ll::co
xxxdl:ldxxxxkkxocldkkkkkkkkocoxOOOOOOOkdcoxO000000kocok000000kdccdk00000ko:cdk00
oxxxxxxxxkddxkkkkkkkkkdxkkkkOOOOOOxOOOOO00OO0Ok0000000000OO0000000000O0000000000
',:oxkxoc;,,,:oxkkxo:,,,;ldkOOkdc;;;cok000Odl:;:lxO000kdc::cdO0000xoc:lxO0000koc
l;'',;,,,;lo:,,,;;,,;col:;;;c:;;;col:;;:lc;;:loc:;:co::;:oo:;;col:;:lo:::ldl:::l
kkxo:,:lxkOOOkdc;;ldOOOOOkdc;:lxO0000ko:;:oxO000Oxl::cdk0000koc::ox0KK0ko::cok0K
kkkkOkOOOOOkOOOOOOOOOOOOOOOOOO0000000000O0000000000000000000000O000KKKKKK0OKKKKK
,:lxOOOOxl:,:okOOOOkdl;:lxO0000Oxl:cdk00000Odlcok000000koclxO00000OdllxOKKKK0kol
l;,,;lc;,,;c;,,;lo:;;;cc;;;cdoc;;;l:;;:oxoc::cc:::lxxl:::l:::cdxo:::lc::ldxoc:cl
KKOd:,;cdOXXXOdc;;:okKXXKko:;;cdOXNNKxl:::lkKNNXOo:::cdONNN0xc:::oOXNN0xc::cx0NW
XXXXX0KXXXXXXXXXK0XXXXXXNNNX0KNNNNNNNNNX0XNNNNNNNNN0KNNNNNNNNNK0NNNNNNNWNKKWWWWW
:lxKXXXXXOdcokKXXXXNKkolxKNNNNNN0xldOXNNNNNXOookXNNNNWN0xokKNNNNNNKxoxKWWNWWXOod
:;,,cdxl;,;:;;;cxOdc;;::;;:dOOo:;:c:::lk0xl::cc::lx0ko:::c::cd0Odc::c::cx0ko::lc
OOxl:,,;cdk0Oxo:;;;:ok00Odl:;;:lxO00koc:::ldO00kdl:::cok0KOxl:::cok0KOxl:::lx0KK
00000kxO00000000OxO000000000kk000000000Ok0KK00KKKK0kOKKKKKKKK0kOKKKKKKKK0k0KKKKK
:cok00000OxllxO000000koldO000000Odlok0KKKKKOxoox0KKKKK0koox0KKKKK0xoox0KKKKKkdld
;:,,:oxoc;;;;;;cokdl:;;:;;coxxoc::c:::lxkdc::c:::ldkdl::cc::ldkdl::lc::lxxoc:loc
OOkdc;;;:oxOOkoc;;;:lxO0Odl:;::lxO00koc:::lxO00kdl:::lxO00Odl::cox0KKOdl:cox0KK0
OOOOOOxk00000000Oxk000000000kk000000000Ok0KK0000KK0k0KKKKKKKK0OKKKKKKKKK00KKK0KK
c:ldOOOO0Oxoldk000000koldk000000kdlox0000K0OdloxOKK0K0kdlox0KKKK0xocok0KKK0xocld
;l:;;cooc;;;c:;:lddl:;:c:::ldxl:::lc::cdxo::coc::cddl::col::cddl:codlccldlccoxdc
000Odl;;:ok000koc;;cok0K0kdl::cdk0KKOxo::ldOKKK0xoccox0KKK0kocldOKKKK0xooxOKKKKK
0000000O0000000000O0KKK0KKKK00KKKK0KKKKK0KKKK0KKKKKKKKKK0KKKKKKKKKO0KKKKKKKKOkKK
c::ldO000Oxl:cok0KKKOxl:cdk0KKKOdl:cok0KK0kdl:cok0KK0xoccldk0K0kocccldOK0kocccco
;;;;;;cxl;;;;::::okc::::::::dxc::::::::odc::::::::ol:ccllcccclcccodocccccccdkklc

Hello dear player!  Won't you please come help me get my wish!
I'm searching teacher's database, but all I find are fish!
Do all his boating trips effect some database dilution?
It should not be this hard for me to find the quiz solution!

Find the solution hidden in the MongoDB on this system.

elf@00163923dc57:~$ ps axuwww
USER       PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
elf          1  0.6  0.0  18508  3388 pts/0    Ss   11:01   0:00 /bin/bash
mongo        9 30.4  0.1 1018680 61820 ?       Sl   11:01   0:01 /usr/bin/mongod --quiet --fork --port 12121 --bind_ip 127.0.0.1 --logpath=/tmp/mongo.log
elf         48  0.0  0.0  34400  2928 pts/0    R+   11:01   0:00 ps axuwww
elf@00163923dc57:~$ mongo
mongo         mongodump     mongofiles    mongoperf     mongorestore  mongostat
mongod        mongoexport   mongoimport   mongoreplay   mongos        mongotop
elf@00163923dc57:~$ mongo --port 12121
MongoDB shell version v3.6.3
connecting to: mongodb://127.0.0.1:12121/
MongoDB server version: 3.6.3
Welcome to the MongoDB shell.
For interactive help, type "help".
For more comprehensive documentation, see
        http://docs.mongodb.org/
Questions? Try the support group
        http://groups.google.com/group/mongodb-user
Server has startup warnings: 
2019-12-15T11:01:06.706+0000 I CONTROL  [initandlisten] 
2019-12-15T11:01:06.707+0000 I CONTROL  [initandlisten] ** WARNING: Access control is not enabled for the database.
2019-12-15T11:01:06.707+0000 I CONTROL  [initandlisten] **          Read and write access to data and configuration is unrestricted.
2019-12-15T11:01:06.707+0000 I CONTROL  [initandlisten] 
2019-12-15T11:01:06.707+0000 I CONTROL  [initandlisten] 
2019-12-15T11:01:06.707+0000 I CONTROL  [initandlisten] ** WARNING: /sys/kernel/mm/transparent_hugepage/enabled is 'always'.
2019-12-15T11:01:06.707+0000 I CONTROL  [initandlisten] **        We suggest setting it to 'never'
2019-12-15T11:01:06.707+0000 I CONTROL  [initandlisten] 
> db
test
> show dbs
admin  0.000GB
elfu   0.000GB
local  0.000GB
test   0.000GB
> use elfu
switched to db elfu
> db.getCollectionNames()
[
        "bait",
        "chum",
        "line",
        "metadata",
        "solution",
        "system.js",
        "tackle",
        "tincan"
]
> db.solution.find()
{ "_id" : "You did good! Just run the command between the stars: ** db.loadServerScripts();displaySolution(); **" }
> db.loadServerScripts()
> ;displaySolution();
[...]
  Congratulations!!
[...]
          .
       __/ __
            /
       /.'*'. 
        .o.'.
       .'.'*'.
      *'.o.'.*.
     .'.*.'.'.*.
    .*.'.o.'.*.'.
       [_____]
        ___/


  Congratulations!!
[...]
          .
       __/ __
            /
       /.'o'. 
        .*.'.
       .'.'*'.
      o'.*.'.o.
     .'.*.'.'.*.
    .*.'.o.'.*.'.
       [_____]
        ___/


  Congratulations!!
[...]
          .
       __/ __
            /
       /.'*'. 
        .o.'.
       .'.'*'.
      o'.o.'.o.
     .'.o.'.'.*.
    .o.'.*.'.o.'.
       [_____]
        ___/


  Congratulations!!
[...]
          .
       __/ __
            /
       /.'*'. 
        .o.'.
       .'.'*'.
      o'.*.'.*.
     .'.o.'.'.*.
    .*.'.o.'.*.'.
       [_____]
        ___/


  Congratulations!!
[...]
          .
       __/ __
            /
       /.'o'. 
        .o.'.
       .'.'*'.
      o'.*.'.o.
     .'.o.'.'.*.
    .*.'.*.'.*.'.
       [_____]
        ___/


  Congratulations!!
[...]
          .
       __/ __
            /
       /.'o'. 
        .*.'.
       .'.'*'.
      *'.o.'.*.
     .'.*.'.'.*.
    .o.'.*.'.*.'.
       [_____]
        ___/


  Congratulations!!
[...]
          .
       __/ __
            /
       /.'*'. 
        .o.'.
       .'.'*'.
      o'.o.'.o.
     .'.o.'.'.o.
    .*.'.o.'.*.'.
       [_____]
        ___/


  Congratulations!!
[...]
          .
       __/ __
            /
       /.'o'. 
        .o.'.
       .'.'*'.
      o'.*.'.o.
     .'.*.'.'.o.
    .o.'.*.'.o.'.
       [_____]
        ___/


  Congratulations!!
[...]
          .
       __/ __
            /
       /.'o'. 
        .o.'.
       .'.'*'.
      o'.*.'.*.
     .'.o.'.'.*.
    .o.'.o.'.o.'.
       [_____]
        ___/


  Congratulations!!
[...]
          .
       __/ __
            /
       /.'*'. 
        .*.'.
       .'.'o'.
      *'.o.'.*.
     .'.*.'.'.o.
    .*.'.*.'.o.'.
       [_____]
        ___/


  Congratulations!!
[...]
          .
       __/ __
            /
       /.'*'. 
        .o.'.
       .'.'*'.
      *'.*.'.o.
     .'.*.'.'.*.
    .o.'.*.'.*.'.
       [_____]
        ___/


  Congratulations!!
[...]
          .
       __/ __
            /
       /.'*'. 
        .o.'.
       .'.'o'.
      *'.*.'.*.
     .'.*.'.'.o.
    .*.'.o.'.*.'.
       [_____]
        ___/


  Congratulations!!
[...]
          .
       __/ __
            /
       /.'*'. 
        .o.'.
       .'.'o'.
      *'.o.'.*.
     .'.*.'.'.*.
    .o.'.o.'.o.'.
       [_____]
        ___/


  Congratulations!!
[...]
          .
       __/ __
            /
       /.'o'. 
        .o.'.
       .'.'o'.
      o'.o.'.*.
     .'.o.'.'.*.
    .o.'.o.'.o.'.
       [_____]
        ___/
  Congratulations!!
> 
```

# Xmas Cheer Laser

From Sparkle Redberry.

```
🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲
🗲                                                                                🗲
🗲 Elf University Student Research Terminal - Christmas Cheer Laser Project       🗲
🗲 ------------------------------------------------------------------------------ 🗲
🗲 The research department at Elf University is currently working on a top-secret 🗲
🗲 Laser which shoots laser beams of Christmas cheer at a range of hundreds of    🗲
🗲 miles. The student research team was successfully able to tweak the laser to   🗲
🗲 JUST the right settings to achieve 5 Mega-Jollies per liter of laser output.   🗲
🗲 Unfortunately, someone broke into the research terminal, changed the laser     🗲
🗲 settings through the Web API and left a note behind at /home/callingcard.txt.  🗲
🗲 Read the calling card and follow the clues to find the correct laser Settings. 🗲
🗲 Apply these correct settings to the laser using it's Web API to achieve laser  🗲
🗲 output of 5 Mega-Jollies per liter.                                            🗲
🗲                                                                                🗲
🗲 Use (Invoke-WebRequest -Uri http://localhost:1225/).RawContent for more info.  🗲
🗲                                                                                🗲
🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲🗲 
```

```
PS /home/elf> (Invoke-WebRequest -Uri http://localhost:1225/).RawContent
HTTP/1.0 200 OK                                                                           
Server: Werkzeug/0.16.0                                                                   
Server: Python/3.6.9                                                                      
Date: Sat, 21 Dec 2019 10:57:38 GMT                                                       
Content-Type: text/html; charset=utf-8
Content-Length: 860
<html>
<body>
<pre>
----------------------------------------------------
Christmas Cheer Laser Project Web API
----------------------------------------------------
Turn the laser on/off:
GET http://localhost:1225/api/on
GET http://localhost:1225/api/off
Check the current Mega-Jollies of laser output
GET http://localhost:1225/api/output
Change the lense refraction value (1.0 - 2.0):
GET http://localhost:1225/api/refraction?val=1.0
Change laser temperature in degrees Celsius:
GET http://localhost:1225/api/temperature?val=-10
Change the mirror angle value (0 - 359):
GET http://localhost:1225/api/angle?val=45.1
Change gaseous elements mixture:
POST http://localhost:1225/api/gas
POST BODY EXAMPLE (gas mixture percentages):
O=5&H=5&He=5&N=5&Ne=20&Ar=10&Xe=10&F=20&Kr=10&Rn=10
----------------------------------------------------
</pre>
</body>
</html>
```

We must find correct `angle`, `refraction`, `temperature` and `gas` values.

First clue is in `/home/callingcard.txt` file:

```
PS /home/elf> gc /home/callingcard.txt
What's become of your dear laser?
Fa la la la la, la la la la
Seems you can't now seem to raise her!
Fa la la la la, la la la la
Could commands hold riddles in hist'ry?
Fa la la la la, la la la la
Nay! You'll ever suffer myst'ry!
Fa la la la la, la la la la
```

Let's see the command history:

```
PS /home/elf> history
  Id CommandLine
  -- -----------
   1 Get-Help -Name Get-Process 
   2 Get-Help -Name Get-* 
   3 Set-ExecutionPolicy Unrestricted 
   4 Get-Service | ConvertTo-HTML -Property Name, Status > C:\services.htm 
   5 Get-Service | Export-CSV c:\service.csv 
   6 Get-Service | Select-Object Name, Status | Export-CSV c:\service.csv 
   7 (Invoke-WebRequest http://127.0.0.1:1225/api/angle?val=65.5).RawContent
   8 Get-EventLog -Log "Application" 
   9 I have many name=value variables that I share to applications system wide. At a com…
  10 gc /home/callingcard.txt
```

The correct `angle` value is `65.5`.

There seems to be another clue in the history, but it's truncated. Let's see the whole line:

```
PS /home/elf> history | Format-Table -Wrap
  Id CommandLine
  -- -----------
   1 Get-Help -Name Get-Process 
   2 Get-Help -Name Get-* 
   3 Set-ExecutionPolicy Unrestricted 
   4 Get-Service | ConvertTo-HTML -Property Name, Status > C:\services.htm 
   5 Get-Service | Export-CSV c:\service.csv 
   6 Get-Service | Select-Object Name, Status | Export-CSV c:\service.csv 
   7 (Invoke-WebRequest http://127.0.0.1:1225/api/angle?val=65.5).RawContent
   8 Get-EventLog -Log "Application" 
   9 I have many name=value variables that I share to applications system wide. At a
     command I will reveal my secrets once you Get my Child Items.
  10 gc /home/callingcard.txt
  11 history
  12 (Invoke-WebRequest -Uri http://localhost:1225/).RawContent
```

Let's check the environment variables:

```
PS /home/elf> Get-ChildItem Env:                               
Name                           Value
----                           -----
_                              /bin/su
DOTNET_SYSTEM_GLOBALIZATION_I… false
HOME                           /home/elf
HOSTNAME                       8ead0accb1ab
LANG                           en_US.UTF-8
LC_ALL                         en_US.UTF-8
LOGNAME                        elf
MAIL                           /var/mail/elf
PATH                           /opt/microsoft/powershell/6:/usr/local/sbin:/usr/local/bi…
PSModuleAnalysisCachePath      /var/cache/microsoft/powershell/PSModuleAnalysisCache/Mod…
PSModulePath                   /home/elf/.local/share/powershell/Modules:/usr/local/shar…
PWD                            /home/elf
RESOURCE_ID                    5709722a-677d-4ac7-9126-d602a1f1d2cd
riddle                         Squeezed and compressed I am hidden away. Expand me from …
SHELL                          /home/elf/elf
SHLVL                          1
TERM                           xterm
USER                           elf
USERDOMAIN                     laserterminal
userdomain                     laserterminal
USERNAME                       elf
username                       elf
```

There's environment variable `riddle`, but it's value is truncated. Let's show it:

```
PS /home/elf> echo $env:riddle
Squeezed and compressed I am hidden away. Expand me from my prison and I will show you the
 way. Recurse through all /etc and Sort on my LastWriteTime to reveal im the newest of all
.
```

Recursively list all entries in `/etc` and sort by `LastWriteTime`:

```
PS /home/elf> Get-ChildItem /etc/ -Recurse| sort LastWriteTime | select -last 1
Get-ChildItem : Access to the path '/etc/ssl/private' is denied.

    Directory: /etc/apt
Mode                LastWriteTime         Length Name
----                -------------         ------ ----
--r---          12/21/19 10:54 AM        5662902 archive
```

Let's extract it:

```
Expand-Archive -LiteralPath /etc/apt/archive -DestinationPath ./archive
```

Go inside and see what's there:

```
PS /home/elf> cd ./archive/refraction/
PS /home/elf/archive/refraction> dir
    Directory: /home/elf/archive/refraction
Mode                LastWriteTime         Length Name
----                -------------         ------ ----
------           11/7/19 11:57 AM            134 riddle
------           11/5/19  2:26 PM        5724384 runme.elf
```

Let's try to run `runme.elf`:

```
PS /home/elf/archive/refraction> Start-Process ./runme.elf
Start-Process : Permission denied
At line:1 char:1
+ Start-Process ./runme.elf
+ ~~~~~~~~~~~~~~~~~~~~~~~~~
+ CategoryInfo          : NotSpecified: (:) [Start-Process], Win32Exception
+ FullyQualifiedErrorId : System.ComponentModel.Win32Exception,Microsoft.PowerShell.Comman
ds.StartProcessCommand
```

Permission denied... Let's add executable bit and try again:

```
PS /home/elf/archive/refraction> chmod +x ./runme.elf
PS /home/elf/archive/refraction> Start-Process ./runme.elf
refraction?val=1.867
```

The correct `refraction` value is `1.867`.

Now, let's see the content of `riddle` file:

```
PS /home/elf/archive/refraction> gc ./riddle
Very shallow am I in the depths of your elf home. You can find my entity by using my md5 i
dentity:
25520151A320B5B0D21561F92C8F6224
```

Let's find the file with the specified MD5 hash:

```
PS /home/elf/archive/refraction> Get-ChildItem -File /home/elf -Recurse | Get-FileHash -Al
gorithm MD5 | Where-Object hash -eq 25520151A320B5B0D21561F92C8F6224 | Select path
Path
----
/home/elf/depths/produce/thhy5hll.txt
```

And check its contents:

```
PS /home/elf/archive/refraction> gc /home/elf/depths/produce/thhy5hll.txt
temperature?val=-33.5
I am one of many thousand similar txt's contained within the deepest of /home/elf/depths. 
Finding me will give you the most strength but doing so will require Piping all the FullNa
me's to Sort Length.
```

The correct `temperature` value is `33.5`.

Let's recusively list all the files in `/home/elf/depths` and sort by the name length:

```
PS /home/elf> Get-ChildItem ./depths/ -Recurse -Name | sort Length | select -last 1
larger/cloud/behavior/beauty/enemy/produce/age/chair/unknown/escape/vote/long/writer/behin
d/ahead/thin/occasionally/explore/tape/wherever/practical/therefore/cool/plate/ice/play/tr
uth/potatoes/beauty/fourth/careful/dawn/adult/either/burn/end/accurate/rubbed/cake/main/sh
e/threw/eager/trip/to/soon/think/fall/is/greatest/become/accident/labor/sail/dropped/fox/0
jhj5xz6.txt
```

Let's see what's inside:

```
PS /home/elf> gc /home/elf/depths/larger/cloud/behavior/beauty/enemy/produce/age/chair/unk
nown/escape/vote/long/writer/behind/ahead/thin/occasionally/explore/tape/wherever/practica
l/therefore/cool/plate/ice/play/truth/potatoes/beauty/fourth/careful/dawn/adult/either/bur
n/end/accurate/rubbed/cake/main/she/threw/eager/trip/to/soon/think/fall/is/greatest/become
/accident/labor/sail/dropped/fox/0jhj5xz6.txt
Get process information to include Username identification. Stop Process to show me you're
 skilled and in this order they must be killed:
bushy
alabaster
minty
holly
Do this for me and then you /shall/see .
```

First, let's show the running processes with their owners:

```
PS /home/elf> Get-Process -IncludeUserName
     WS(M)   CPU(s)      Id UserName                       ProcessName
     -----   ------      -- --------                       -----------
     28.65     0.61       6 root                           CheerLaserServi
    129.34     4.37      31 elf                            elf
      3.43     0.02       1 root                           init
      0.78     0.00      24 bushy                          sleep
      0.78     0.00      25 alabaster                      sleep
      0.72     0.00      28 minty                          sleep
      0.72     0.00      29 holly                          sleep
      3.28     0.00      30 root                           su
```

Then, kill them in the correct order and show the contents of `/shall/see`:

```
PS /home/elf> Stop-Process -Id 24
PS /home/elf> Stop-Process -Id 25
PS /home/elf> Stop-Process -Id 28
PS /home/elf> Stop-Process -Id 29
PS /home/elf> gc /shall/see
Get the .xml children of /etc - an event log to be found. Group all .Id's and the last thi
ng will be in the Properties of the lonely unique event Id.
```

First, let's find `.xml` file in `/etc`:

```
PS /home/elf> Get-ChildItem /etc/ -Recurse -Include *.xml -Name
systemd/system/timers.target.wants/EventLog.xml
```

We can eaily find out that there's only one entry with `Id` `1`:

```
PS /home/elf> gc /etc/systemd/system/timers.target.wants/EventLog.xml | Select-String 'N="Id">1'   
      <I32 N="Id">1</I32>
```

Let's show it with some context:

```
PS /home/elf> gc /etc/systemd/system/timers.target.wants/EventLog.xml | Select-String 'N="Id">1' -Context 150
      <I32 N="Id">1</I32>
[...]
<Props>
                <S N="Value">C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe 
-c "`$correct_gases_postbody = @{`n    O=6`n    H=7`n    He=3`n    N=4`n    Ne=22`n    
Ar=11`n    Xe=10`n    F=20`n    Kr=8`n    Rn=9`n}`n"</S>
              </Props>
[...]
```

The correct `gas` value is `O=6&H=7&He=3&N=4&Ne=22&Ar=11&Xe=10&F=20&Kr=8&Rn=9`.

We've got everything we need. Let's use the laser's API to turn it on, set the values and check the output:

```
PS /home/elf> (Invoke-WebRequest -Uri http://localhost:1225/api/on).RawContent
HTTP/1.0 200 OK                                                                           
Server: Werkzeug/0.16.0                                                                   
Server: Python/3.6.9                                                                      
Content-Type: text/html; charset=utf-8
Content-Length: 32

Christmas Cheer Laser Powered On


PS /home/elf> (Invoke-WebRequest -Uri http://localhost:1225/api/angle?val=65.5).RawContent

HTTP/1.0 200 OK                                                                           
Server: Werkzeug/0.16.0                                                                   
Date: Sat, 21 Dec 2019 11:31:09 GMT                                                       
Content-Type: text/html; charset=utf-8
Content-Length: 77

Updated Mirror Angle - Check /api/output if 5 Mega-Jollies per liter reached.


PS /home/elf> (Invoke-WebRequest -Uri http://localhost:1225/api/refraction?val=1.867).RawContent
HTTP/1.0 200 OK                                                                           
Server: Werkzeug/0.16.0                                                                   
Server: Python/3.6.9                                                                      
Date: Sat, 21 Dec 2019 11:31:17 GMT                                                       
Content-Type: text/html; charset=utf-8
Content-Length: 87
Updated Lense Refraction Level - Check /api/output if 5 Mega-Jollies per liter reached.


PS /home/elf> (Invoke-WebRequest -Uri http://localhost:1225/api/temperature?val=-33.5).RawContent
HTTP/1.0 200 OK                                                                           
Server: Werkzeug/0.16.0                                                                   
Server: Python/3.6.9                                                                      
Date: Sat, 21 Dec 2019 11:31:24 GMT                                                       
Content-Type: text/html; charset=utf-8
Content-Length: 82
Updated Laser Temperature - Check /api/output if 5 Mega-Jollies per liter reached.


PS /home/elf> (Invoke-WebRequest -Uri http://localhost:1225/api/gas -Method POST -Body 'O=6&H=7&He=3&N=4&Ne=22&Ar=11&Xe=10&F=20&Kr=8&Rn=9').RawContent
HTTP/1.0 200 OK                                                                           
Server: Werkzeug/0.16.0                                                                   
Server: Python/3.6.9                                                                      
Date: Sat, 21 Dec 2019 11:31:58 GMT                                                       
Content-Type: text/html; charset=utf-8
Content-Length: 81
Updated Gas Measurements - Check /api/output if 5 Mega-Jollies per liter reached.


PS /home/elf> (Invoke-WebRequest -Uri http://localhost:1225/api/output).RawContent
HTTP/1.0 200 OK                                                                           
Server: Werkzeug/0.16.0                                                                   
Server: Python/3.6.9                                                                      
Date: Sat, 21 Dec 2019 11:32:16 GMT                                                       
Content-Type: text/html; charset=utf-8
Content-Length: 200
Success! - 5.10 Mega-Jollies of Laser Output Reached!
```

# Smart Braces

From Kent Tinseltooth.

```
Inner Voice: Kent. Kent. Wake up, Kent.
Inner Voice: I'm talking to you, Kent.
Kent TinselTooth: Who said that? I must be going insane.
Kent TinselTooth: Am I?
Inner Voice: That remains to be seen, Kent. But we are having a conversation.
Inner Voice: This is Santa, Kent, and you've been a very naughty boy.
Kent TinselTooth: Alright! Who is this?! Holly? Minty? Alabaster?
Inner Voice: I am known by many names. I am the boss of the North Pole. Turn to me and be 
hired after graduation.
Kent TinselTooth: Oh, sure.
Inner Voice: Cut the candy, Kent, you've built an automated, machine-learning, sleigh devi
ce.
Kent TinselTooth: How did you know that?
Inner Voice: I'm Santa - I know everything.
Kent TinselTooth: Oh. Kringle. *sigh*
Inner Voice: That's right, Kent. Where is the sleigh device now?
Kent TinselTooth: I can't tell you.
Inner Voice: How would you like to intern for the rest of time?
Kent TinselTooth: Please no, they're testing it at srf.elfu.org using default creds, but I
 don't know more. It's classified.
Inner Voice: Very good Kent, that's all I needed to know.
Kent TinselTooth: I thought you knew everything?
Inner Voice: Nevermind that. I want you to think about what you've researched and studied.
 From now on, stop playing with your teeth, and floss more.
*Inner Voice Goes Silent*
Kent TinselTooth: Oh no, I sure hope that voice was Santa's.
Kent TinselTooth: I suspect someone may have hacked into my IOT teeth braces.
Kent TinselTooth: I must have forgotten to configure the firewall...
Kent TinselTooth: Please review /home/elfuuser/IOTteethBraces.md and help me configure the
 firewall.
Kent TinselTooth: Please hurry; having this ribbon cable on my teeth is uncomfortable.
```

````
elfuuser@4648d30d21e8:~$ cat /home/elfuuser/IOTteethBraces.md
# ElfU Research Labs - Smart Braces
### A Lightweight Linux Device for Teeth Braces
### Imagined and Created by ElfU Student Kent TinselTooth
This device is embedded into one's teeth braces for easy management and monitoring of dent
al status. It uses FTP and HTTP for management and monitoring purposes but also has SSH fo
r remote access. Please refer to the management documentation for this purpose.
## Proper Firewall configuration:
The firewall used for this system is `iptables`. The following is an example of how to set
 a default policy with using `iptables`:
```
sudo iptables -P FORWARD DROP
```
The following is an example of allowing traffic from a specific IP and to a specific port:
```
sudo iptables -A INPUT -p tcp --dport 25 -s 172.18.5.4 -j ACCEPT
```
A proper configuration for the Smart Braces should be exactly:
1. Set the default policies to DROP for the INPUT, FORWARD, and OUTPUT chains.
2. Create a rule to ACCEPT all connections that are ESTABLISHED,RELATED on the INPUT and t
he OUTPUT chains.
3. Create a rule to ACCEPT only remote source IP address 172.19.0.225 to access the local 
SSH server (on port 22).
4. Create a rule to ACCEPT any source IP to the local TCP services on ports 21 and 80.
5. Create a rule to ACCEPT all OUTPUT traffic with a destination TCP port of 80.
6. Create a rule applied to the INPUT chain to ACCEPT all traffic from the lo interface.
````

1. Set the default policies to DROP for the INPUT, FORWARD, and OUTPUT chains.
```
sudo iptables -P INPUT DROP
sudo iptables -P FORWARD DROP
sudo iptables -P OUTPUT DROP
```

2. Create a rule to ACCEPT all connections that are ESTABLISHED,RELATED on the INPUT and the OUTPUT chains.
```
sudo iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
```

3. Create a rule to ACCEPT only remote source IP address 172.19.0.225 to access the local SSH server (on port 22).
```
sudo iptables -A INPUT -p tcp --dport 22 -s 172.19.0.225 -j ACCEPT
```

4. Create a rule to ACCEPT any source IP to the local TCP services on ports 21 and 80.
```
sudo iptables -A INPUT -p tcp --dport 21 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 80 -j ACCEPT
```

5. Create a rule to ACCEPT all OUTPUT traffic with a destination TCP port of 80.
```
sudo iptables -A OUTPUT -p tcp --dport 80 -j ACCEPT
```

6. Create a rule applied to the INPUT chain to ACCEPT all traffic from the lo interface.
```
sudo iptables -A INPUT -i lo -j ACCEPT
```

```
Kent TinselTooth: Great, you hardened my IOT Smart Braces firewall!
```

# Frosty Keypad

From Tangle Coalbox.

We're given the following hint and we need to guess the code:

> One digit is repeated once, it's prime, and you can see which keys were used

By looking at the keypad, we can see that `1`, `3` and `7` digits are used. One digit is repeated once, so we must find 4-digit prime with `1`, `3` and `7` digits only. The following command generates the list of matching numbers:

```
$ python -c 'print([x for x in range(1000,9999) if (not [t for t in range(2,x) if not x%t]) and (not [t for t in "1 3 7".split() if t not in str(x)]) and ((not [t for t in "0 2 4 5 6 8 9".split() if t in str(x)]))])'
[1373, 1733, 3137, 3371, 7331]
```

By trying all of them, we can find out that the correct code is `7331`.

# Graylog

From Pepper Minstix.

We have to analyze the incident.

**Question 1:**

Minty CandyCane reported some weird activity on his computer after he clicked on a link in Firefox for a cookie recipe and downloaded a file.

What is the full-path + filename of the first malicious file downloaded by Minty?


*Answer:* `C:\Users\minty\Downloads\cookie_recipe.exe` 

> We can find this searching for sysmon file creation event id `2` with a process named `firefox.exe` and not junk `.temp` files. We can use regular expressions to include or exclude patterns:
> 
> ```
> TargetFilename:/.+\.pdf/
> ```

**Question 2:**

The malicious file downloaded and executed by Minty gave the attacker remote access to his machine. What was the `ip:port` the malicious file connected to first?


*Answer*: `192.168.247.175:4444` 

> We can pivot off the answer to our first question using the binary path as our `ProcessImage`.


**Question 3:**

What was the first command executed by the attacker?

(answer is a single word)


*Answer*: `whoami` 

> Since all commands (sysmon event id `1`) by the attacker are initially running through the `cookie_recipe.exe` binary, we can set its full-path as our `ParentProcessImage` to find child processes it creates sorting on timestamp.


**Question 4:**

What is the one-word service name the attacker used to escalate privileges?


*Answer*: `webexservice`

> Continuing on using the `cookie_reciper.exe` binary as our `ParentProcessImage`, we should see some more commands later on related to a service.


**Question 5:**

What is the file-path + filename of the binary ran by the attacker to dump credentials?


*Answer*: `C:\cookie.exe`

> The attacker elevates privileges using the vulnerable webexservice to run a file called `cookie_recipe2.exe`. Let's use this binary path in our `ParentProcessImage` search.


**Question 6:**

The attacker pivoted to another workstation using credentials gained from Minty's computer. Which account name was used to pivot to another machine?


*Answer*: `alabaster` 

> Windows Event Id `4624` is generated when a user network logon occurs successfully. We can also filter on the attacker's IP using `SourceNetworkAddress`.


**Question 7:**

What is the time ( HH:MM:SS ) the attacker makes a Remote Desktop connection to another machine?


*Answer*: `06:04:28`

> `LogonType 10` is used for successful network connections using the RDP client.


**Question 8:**

The attacker navigates the file system of a third host using their Remote Desktop Connection to the second host. What is the `SourceHostName,DestinationHostname,LogonType` of this connection?

(submit in that order as csv)


*Answer*: `elfu-res-wks2,elfu-res-wks3,3`

> The attacker has GUI access to workstation 2 via RDP. They likely use this GUI connection to access the file system of of workstation 3 using `explorer.exe` via UNC file paths (which is why we don't see any `cmd.exe` or `powershell.exe` process creates). However, we still see the successful network authentication for this with event id `4624` and logon type `3`.


**Question 9:**

What is the full-path + filename of the secret research document after being transferred from the third host to the second host?


*Answer*: `C:\Users\alabaster\Desktop\super_secret_elfu_research.pdf`

> We can look for sysmon file creation event id of `2` with a source of workstation 2. We can also use regex to filter out overly common file paths using something like:
> 
> ```
> AND NOT TargetFilename:/.+AppData.+/
> ```


**Question 10:**

What is the IPv4 address (as found in logs) the secret research document was exfiltrated to?


*Answer*: `104.22.3.84`

> We can look for the original document in `CommandLine` using regex.
> 
> When we do that, we see a long a long PowerShell command using `Invoke-Webrequest` to a remote URL of `https://pastebin.com/post.php`.
> 
> We can pivot off of this information to look for a sysmon network connection id of `3` with a source of `elfu-res-wks2` and `DestinationHostname` of `pastebin.com`.

# Holiday Hack Trail

From Minty Candycane.

**Easy**

Just play with `GET` parameters in URL (e.g. increase the amount of money and set distance to 7999).

**Medium**

Same as easy, but this time parameters are sent in `POST`.

**Hard**

TBD