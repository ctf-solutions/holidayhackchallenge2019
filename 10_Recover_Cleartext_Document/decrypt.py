#!/usr/bin/env python3

from time import strptime
from calendar import timegm
from Crypto.Cipher import DES
import sys

START_TIME = timegm(strptime('2019-12-06T19:00', '%Y-%m-%dT%H:%M'))
END_TIME = timegm(strptime('2019-12-06T21:00', '%Y-%m-%dT%H:%M'))
KEY_LENGTH = 8

SEED = 0

def generate_random_number():
    global SEED
    
    SEED = SEED * 0x343fd + 0x269ec3
    return SEED >> 0x10 & 0x7fff

def generate_key(seed, key_length):
    global SEED

    SEED = seed
    key = bytearray()
    for i in range(8):
        key.append(generate_random_number() & 0xFF)
    return key

with open(sys.argv[1], 'rb') as f:
    encrypted = f.read()

for timestamp in range(START_TIME, END_TIME):
    key = generate_key(timestamp, KEY_LENGTH)
    cipher = DES.new(key, DES.MODE_CBC, iv=b'\x00'*8)
    decrypted = cipher.decrypt(encrypted[:8])
    try:
        if decrypted[:5].decode('ascii') == '%PDF-':
            with open(sys.argv[1][:-4], 'wb') as f:
                f.write(decrypted + cipher.decrypt(encrypted[8:]))
            sys.exit(0)
    except UnicodeDecodeError:
        continue